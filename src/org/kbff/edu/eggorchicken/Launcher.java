package org.kbff.edu.eggorchicken;

public class Launcher {
		public static void main(String[] args) {
		MyThread chicken = new MyThread("Курица");
		MyThread egg = new MyThread("Яйцо");
		
		chicken.setOpponent(egg);
		egg.setOpponent(chicken);
		
		chicken.start();
		egg.start();
		
		try {
			chicken.join();
			egg.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
