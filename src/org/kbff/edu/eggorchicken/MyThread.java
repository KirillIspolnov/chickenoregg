package org.kbff.edu.eggorchicken;

public class MyThread extends Thread {
	private MyThread other;
	private int counter = 0;
	private String me;
	
	public MyThread(String me) {
		this.me = me;
	}
	
	public void setOpponent(MyThread opponent) {
		other = opponent;
	}
	
	public void run() {
		while((counter++) < 5_000) {
			System.out.println(me);
		}
		
		if(!other.isAlive()) {
			System.out.printf("Побеждает %s", me);
		}
	}
}
